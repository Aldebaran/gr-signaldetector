/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_SIGNALDETECTOR_SNR_CALC_H
#define INCLUDED_SIGNALDETECTOR_SNR_CALC_H

#include <signaldetector/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace signaldetector {

    /*!
     * \brief <+description of block+>
     * \ingroup signaldetector
     *
     */
    class SIGNALDETECTOR_API snr_calc : virtual public gr::block
    {
     public:
      typedef boost::shared_ptr<snr_calc> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of signaldetector::snr_calc.
       *
       * To avoid accidental use of raw pointers, signaldetector::snr_calc's
       * constructor is in a private implementation
       * class. signaldetector::snr_calc::make is the public interface for
       * creating new instances.
       */
      static sptr make();
    };

  } // namespace signaldetector
} // namespace gr

#endif /* INCLUDED_SIGNALDETECTOR_SNR_CALC_H */

