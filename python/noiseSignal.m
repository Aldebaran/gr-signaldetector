function noiseSignal(len,B)

%length is the length of input vector
%B is bandwidth of CW signal

%Ynoise1 = rand(len,1) + 2;
Ynoise = rand(len,1) + 2;

t1 = 0:(pi/(B-2)): pi/2;
Ysignal = cos(t1);

a = length(Ynoise)/2;
b = length(Ynoise)/2 + 24;

% pristeje signal sumu
z=1;
for i=a:b
    
    Ynoise(i) = (Ysignal(z)+1)*200;
    z = z +1;
end
z=25;
for i=a-25:a-1
    Ynoise(i) = (Ysignal(z)+1)*200;
    z = z-1;
end

% AvgNoise calc
len = length(Ynoise);
summ = 0;
noisecnt = 0;
[pick,pickIx] = max(Ynoise);


for i=1: pickIx-30-1
   summ = summ +Ynoise(i);
   noisecnt = noisecnt+1;       
end
for i = (pickIx +30+1) : len
    summ = summ +Ynoise(i);
    noisecnt=noisecnt+1;       
end
avgNoise = summ/noisecnt

%SignalPowercalculation
summCW = 0;
CWcnt = 0;
for i = pickIx-30 : pickIx+30
    summCW = summCW + (Ynoise(i)- avgNoise);
    CWcnt = CWcnt+1;
end
%Outputs
summCW
CWcnt
pickIx
SNR = 10 * log10(summCW);
Signal = Ynoise;

%Write SNR to document
fName = 'snr.txt'
fid = fopen(fName,'w');
fprintf(fid,'%2.10f',SNR);
fclose(fid);

%Write signal samples to document
for i=1:length(Ynoise)
    if i==1
        fid1 = fopen('signal.txt', 'w');
        fprintf(fid1, "%3.10f\n", Ynoise(i));
        fclose(fid1);
    else
        fid1 = fopen('signal.txt', 'a');
        fprintf(fid1, "%3.10f\n", Ynoise(i));
        fclose(fid1);
    end
end

plot(0:length(Ynoise)-1, Ynoise)
