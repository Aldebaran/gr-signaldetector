/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "snr_calc_impl.h"
#include <algorithm>
#include <vector>
#include <math.h>
#include <iostream>

namespace gr {
  namespace signaldetector {

    snr_calc::sptr
    snr_calc::make()
    {
      return gnuradio::get_initial_sptr
        (new snr_calc_impl());
    }

    /*
     * The private constructor
     */
    snr_calc_impl::snr_calc_impl()
      : gr::block("snr_calc",
              gr::io_signature::make(1, 1, sizeof(float)*32768),
              gr::io_signature::make(1, 1, sizeof(float)))
    {}

    /*
     * Our virtual destructor.
     */
    snr_calc_impl::~snr_calc_impl()
    {
    }

    void
    snr_calc_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
        ninput_items_required[0] = noutput_items;
        /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */
    }

    int
    snr_calc_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
        const float *in = (const float *) input_items[0];
        float *out = (float *) output_items[0];

        //auto Ix = std::distance(in.begin(), std::max_element(in[0], in[in.size()]));

        int Ix = 0;
        int maxValue = 0;
        //int abc = in.length();
        for(int i = 0; i < 32768; i++){
          if(in[i] > maxValue){
            maxValue = in[i];
            Ix = i;
          }
        }
        int Bs = Ix - 30;
        int Bf = Ix + 30;
        double summ = 0;
        double noisecnt = 0;
        for (int i = 0; i < Bs; i++){
          summ= summ + in[i];
          noisecnt++;
        }
        for (int i = Bf+1; i <= 32768; i++){
          summ = summ + in[i];
          noisecnt++;
        }
        double avgNoise = summ/noisecnt;

        double summCW = 0;
        int k;
        int counter = 0;
        for(k = Bs; k <= Bf; k++){
          summCW = summCW + (in[k] - avgNoise);
          counter++;
        }
        //out[0] = (float) summCW;
        out[0] = (float)10*log10(summCW);


        // Do <+signal processing+>
        // Tell runtime system how many input items we consumed on
        // each input stream.
        consume_each (noutput_items);

        // Tell runtime system how many output items we produced.
        return noutput_items;
    }

  } /* namespace signaldetector */
} /* namespace gr */

