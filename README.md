

gr-signaldetector beta

WHAT IS GR-SIGNALDETECTOR?
    Signaldetector is a collection of signal processing blocks for GNURadio
    that are used to easily build a satellite beacon receiver using
    GNURadio and USRP.

DOCUMENTATION
    Signaldetector block grabs a signals spectrum vector (length > 1024 
    samples) on the input port, finds a signal in a spectrum and 
    calculates its power. Output is signal strength in log scale as
    a scalar value.

INSTALLING
    1. Clone the repository
	    git clone http://repo.ijs.si/satprosi/gr-signaldetector.git

    2. Create and change to directory gr-signaldetector/build
		$cd gr-signaldetector
        gr-signaldetector$mkdir build
	    gr-signaldetector$cd build

    3. CMake
	    gr-signaldetector/build$cmake ../
        
    4. Make the thing
	    gr-signaldetector/build$make
        
    5. Install
	    gr-signaldetector/build$sudo make install
        
    6. Run ldconfig to create links to the installed shared object.
        gr-signaldetector/build$ldconfig
        

REQUIREMENTS
    GNURadio v3.7.4

CONFIGURATION
    No configration is possible

