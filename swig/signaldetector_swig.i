/* -*- c++ -*- */

#define SIGNALDETECTOR_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "signaldetector_swig_doc.i"

%{
#include "signaldetector/snr_calc.h"
%}


%include "signaldetector/snr_calc.h"
GR_SWIG_BLOCK_MAGIC2(signaldetector, snr_calc);
