# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/sebastijan/custom_blocks/gr-signaldetector/lib/qa_signaldetector.cc" "/home/sebastijan/custom_blocks/gr-signaldetector/build/lib/CMakeFiles/test-signaldetector.dir/qa_signaldetector.cc.o"
  "/home/sebastijan/custom_blocks/gr-signaldetector/lib/test_signaldetector.cc" "/home/sebastijan/custom_blocks/gr-signaldetector/build/lib/CMakeFiles/test-signaldetector.dir/test_signaldetector.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/sebastijan/custom_blocks/gr-signaldetector/build/lib/CMakeFiles/gnuradio-signaldetector.dir/DependInfo.cmake"
  )
