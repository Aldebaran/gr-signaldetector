# CMake generated Testfile for 
# Source directory: /home/sebastijan/custom_blocks/gr-signaldetector/lib
# Build directory: /home/sebastijan/custom_blocks/gr-signaldetector/build/lib
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(test_signaldetector "/bin/sh" "/home/sebastijan/custom_blocks/gr-signaldetector/build/lib/test_signaldetector_test.sh")
