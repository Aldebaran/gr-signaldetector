# CMake generated Testfile for 
# Source directory: /home/sebastijan/custom_blocks/gr-signaldetector
# Build directory: /home/sebastijan/custom_blocks/gr-signaldetector/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(include/signaldetector)
SUBDIRS(lib)
SUBDIRS(swig)
SUBDIRS(python)
SUBDIRS(grc)
SUBDIRS(apps)
SUBDIRS(docs)
