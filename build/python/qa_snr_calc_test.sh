#!/bin/sh
export GR_DONT_LOAD_PREFS=1
export srcdir=/home/sebastijan/custom_blocks/gr-signaldetector/python
export PATH=/home/sebastijan/custom_blocks/gr-signaldetector/build/python:$PATH
export LD_LIBRARY_PATH=/home/sebastijan/custom_blocks/gr-signaldetector/build/lib:$LD_LIBRARY_PATH
export PYTHONPATH=/home/sebastijan/custom_blocks/gr-signaldetector/build/swig:$PYTHONPATH
/usr/bin/python /home/sebastijan/custom_blocks/gr-signaldetector/python/qa_snr_calc.py 
